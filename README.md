This project was moved to https://gitlab.com/frederic-zinelli/mkdocs-addresses

The project on PyPI: https://pypi.org/project/mkdocs-addresses/

Related documentation on: https://frederic-zinelli.gitlab.io/mkdocs-addresses/

---

Old links are permanently redirected where appropriate (301).
